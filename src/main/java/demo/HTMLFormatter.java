package demo;

import org.apache.log4j.Logger;
import picture.processplatform.shares.PortalEntry;
import picture.processplatform.shares.PortalList;
import ppp.xml.FactSheetXML;
import ppp.xml.ProcessContextElementXML;
import ppp.xml.attributes.*;

import java.io.File;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/*
 * NOTE: Out of simplicity the HTMLFormatter outputs user written text as is.
 * Ideally it should be escaped by using something like the
 * apache-commons StringEscapeUtils.escapeHtml4() method so that
 * it's being displayed correctly in the html documents.
 */
public class HTMLFormatter {
    private static Logger logger = Logger.getLogger(HTMLFormatter.class);
    private String outputDir;

    HTMLFormatter(String outputDir) {
        this.outputDir = outputDir;
    }

    public void writeFactSheetPage(FactSheetXML factsheet, String onlineURL) {
        logger.debug("Writing factsheet html-file: " + factsheet.getName());

        HTMLFileBuilder builder = new HTMLFileBuilder(factsheet.getName())
                .putOpenTag("h1")
                .append(factsheet.getName())
                .append(" ")
                .putLink(onlineURL, "(Online Ansicht)")
                .putCloseTag("h1")
                .putElement("h2", "Technische Attribute")
                .putElement("h3", "UUID")
                .putElement("p", factsheet.getUuid())
                .putElement("h3", "Notation")
                .putElement("p", factsheet.getNotation());

        appendAttributeGroupHTML(builder, factsheet.getAttributeGroups());

        makeDirectory("processes");
        builder.writeToFile(outputDir + "processes/", factsheet.getUuid() + ".html");
    }

    public void writeProcessContextPage(String type, ProcessContextElementXML contextElement) {
        logger.debug("Writing context html-file: " + contextElement.getName());

        HTMLFileBuilder builder = new HTMLFileBuilder(contextElement.getName())
                .putElement("h1", contextElement.getName())
                .putElement("h2", "Technische Attribute")
                .putElement("h3", "UUID")
                .putElement("p", contextElement.getUuid());

        appendAttributeGroupHTML(builder, contextElement.getAttributeGroups());

        makeDirectory(type);
        builder.writeToFile(outputDir + type + "/", contextElement.getUuid() + ".html");
    }

    public void writeIndexPages(PortalList list, Collection<ReferenceXML> referencedProcessContextElements) {
        Map<String, List<ReferenceXML>> typeMap = referencedProcessContextElements
                .stream()
                .sorted(Comparator.comparing(ReferenceXML::getLabel))
                .collect(Collectors.groupingBy(ReferenceXML::getTargetType));

        writeMainIndexPage(list, typeMap.keySet());

        for (Map.Entry<String, List<ReferenceXML>> entry : typeMap.entrySet()) {
            writeContextIndexPage(entry.getKey(), entry.getValue());
        }
    }

    private void writeMainIndexPage(PortalList list, Collection<String> contextTypes) {
        logger.debug("Writing index page...");

        String pageTitle = String.format("Portalfreigabe %s", list.title);
        String title = String.format("Index der Steckbriefdaten aus der Portalfreigabe \"%s\"", list.title);
        HTMLFileBuilder builder = new HTMLFileBuilder(pageTitle)
                .putElement("h1", title)
                .putElement("h2", "Prozesse");

        if (list.entries.isEmpty()) {
            builder.putElement("p", "In der Freigabe sind keine Prozesse enthalten");
        } else {
            builder.putOpenTag("ul");
            for (PortalEntry entry : list.entries) {
                builder.putLinkListElement("./processes/" + entry.id + ".html", entry.label);
            }
            builder.putCloseTag("ul");
        }

        builder.putElement("h2", "Prozesskontexte")
                .putOpenTag("ul");
        for (String type : contextTypes) {
            builder.putLinkListElement(type + "/index.html", type);
        }
        builder.putCloseTag("ul");

        builder.writeToFile(outputDir, "index.html");
    }

    private void writeContextIndexPage(String type, List<ReferenceXML> references) {
        logger.debug("Writing index page for context type: " + type + "...");

        String title = String.format("Prozesskontextelemente des Typs \"%s\"", type);
        String pageTitle = String.format("Prozesskontexte %s", type);
        HTMLFileBuilder builder = new HTMLFileBuilder(pageTitle)
                .putElement("h2", title)
                .putOpenTag("ul");
        for (ReferenceXML reference : references) {
            builder.putLinkListElement(reference.getUuid() + ".html", reference.getLabel());
        }
        builder.putCloseTag("ul");

        builder.writeToFile(outputDir + type + "/", "index.html");
    }

    private void appendAttributeGroupHTML(HTMLFileBuilder builder, List<AttributeGroupXML> attributeGroups) {
        for (AttributeGroupXML group : attributeGroups) {
            builder.putElement("h2", group.getLabel());

            for (AttributeValueXML value : group.getAttributes()) {
                builder.putElement("h3", value.getAttributeID());

                switch (value.getDataType()) {
                    case "text":
                    case "selection":
                        addPlainText(builder, (TextValueXML) value);
                        break;
                    case "richtext":
                        addHtmlFormattedText(builder, (TextValueXML) value);
                        break;
                    case "number":
                    case "computed":
                        addNumericValue(builder, (NumericValueXML) value);
                        break;
                    case "multiselection":
                    case "tags":
                        addList(builder, (ListValueXML) value);
                        break;
                    case "interModelLink":
                    case "references":
                        addReferenceList(builder, (ReferenceListValueXML) value);
                        break;
                    case "distribution":
                        addDistribution(builder, (DistributionValueXML) value);
                        break;
                    case "links":
                    case "fileLinks":
                        addLinks(builder, (LinkListValueXML) value);
                        break;
                }
            }
        }
    }

    private void addPlainText(HTMLFileBuilder builder, TextValueXML textValue) {
        builder.putElement("p", textValue.getValue());
    }

    private void addHtmlFormattedText(HTMLFileBuilder builder, TextValueXML textValue) {
        Pattern cdataPattern = Pattern.compile("^<!\\[CDATA\\[([\\s\\S]+)]]>$");
        Matcher m = cdataPattern.matcher(textValue.getValue());
        if (m.find()) {
            builder.putElement("p", m.group(1));
        }
    }

    private void addNumericValue(HTMLFileBuilder builder, NumericValueXML numericValue) {
        String html = numericValue.getNumber().toString();
        if (numericValue.getUnit() != null) {
            html += " " + numericValue.getUnit();
        }
        builder.putElement("p", html);
    }

    private void addList(HTMLFileBuilder builder, ListValueXML listValue) {
        builder.putElement("p", String.join(", ", listValue.getItems()));
    }

    private void addReferenceList(HTMLFileBuilder builder, ReferenceListValueXML referenceListValue) {
        builder.putOpenTag("ul");
        for (ReferenceXML reference : referenceListValue.getReferences()) {
            String localURL = "../" + reference.getTargetType() + "/" + reference.getUuid() + ".html";
            builder.putLinkListElement(localURL, reference.getLabel());
        }
        builder.putCloseTag("ul");
    }

    private void addDistribution(HTMLFileBuilder builder, DistributionValueXML distributionValue) {
        builder.putOpenTag("ul");
        for (DistributionItemXML distributionItem : distributionValue.getItems()) {
            String html = distributionItem.getLabel() + ": " + distributionItem.getUuid() + " " + distributionItem.getPercent();
            builder.putElement("li", html);
        }
        builder.putCloseTag("ul");
    }

    private void addLinks(HTMLFileBuilder builder, LinkListValueXML linkListValue) {
        builder.putOpenTag("ul");
        for (LinkXML link : linkListValue.getLinks()) {
            builder.putLinkListElement(link.getUrl(), link.getLabel());
        }
        builder.putCloseTag("ul");
    }

    private void makeDirectory(String dir) {
        new File(outputDir + dir).mkdirs();
    }
}
