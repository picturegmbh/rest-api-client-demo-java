package demo;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

public class HTMLFileBuilder {
    private static Logger logger = Logger.getLogger(HTMLFileBuilder.class);
    private static String template = loadTemplate();

    private String html;
    private StringBuilder builder;

    public HTMLFileBuilder(String title) {
        html = template.replace("{title}", title);
        builder = new StringBuilder();
    }

    public String build() {
        return html.replace("{htmlbody}", builder.toString());
    }

    @Override
    public String toString() {
        return this.build();
    }

    public HTMLFileBuilder putLinkListElement(String url, String label) {
        return this.putOpenTag("li")
                .putLink(url, label)
                .putCloseTag("li");
    }

    public HTMLFileBuilder putElement(String tag, String label) {
        return this.putOpenTag(tag)
                .append(label)
                .putCloseTag(tag);
    }

    public HTMLFileBuilder putLink(String url, String label) {
        return this.append("<a href=\"")
                .append(url)
                .append("\">")
                .append(label)
                .append("</a>");
    }

    public HTMLFileBuilder putOpenTag(String tag) {
        return this.append(String.format("<%s>", tag));
    }

    public HTMLFileBuilder putCloseTag(String tag) {
        return this.append(String.format("</%s>", tag));
    }

    public HTMLFileBuilder append(String text) {
        builder.append(text);
        return this;
    }

    public void writeToFile(String outputDir, String filename) {
        try {
            IOUtils.write(this.build(), new FileOutputStream(outputDir + filename), Charset.defaultCharset());
        } catch (IOException e) {
            logger.error("Could not write to file " + filename, e);
        }
    }

    private static String loadTemplate() {
        try {
            return IOUtils.toString(FactSheetDownloader.class.getResource("template.html"), Charset.defaultCharset());
        } catch (IOException e) {
            logger.error("Could not load html template.", e);
            throw new RuntimeException();
        }
    }
}
