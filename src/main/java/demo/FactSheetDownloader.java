package demo;

import org.apache.log4j.Logger;
import picture.processplatform.PortalAPI;
import picture.processplatform.RequestFailedException;
import picture.processplatform.register.ModelNotation;
import picture.processplatform.shares.PortalEntry;
import picture.processplatform.shares.PortalList;
import ppp.xml.FactSheetXML;
import ppp.xml.ProcessContextElementXML;
import ppp.xml.attributes.AttributeGroupXML;
import ppp.xml.attributes.AttributeValueXML;
import ppp.xml.attributes.ReferenceListValueXML;
import ppp.xml.attributes.ReferenceXML;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FactSheetDownloader {
    private static Logger logger = Logger.getLogger(FactSheetDownloader.class);
    private static List<String> processContextTypes =
            Arrays.asList("OrganisationUnit", "Product", "Document", "ExternalParticipant", "Rule", "Software", "Hardware", "Position", "Person", "SalaryBracket");

    private Map<String, ReferenceXML> referencedProcessContextElements = new HashMap<>();
    private String baseUrl;
    private String shareToken;
    private HTMLFormatter formatter;

    public FactSheetDownloader(String baseUrl, String shareToken, String outputDir) {
        this.baseUrl = baseUrl;
        this.shareToken = shareToken;
        this.formatter = new HTMLFormatter(outputDir);
    }

    public void execute() {
        logger.info("Starting factsheet downloader...");

        try (PortalAPI portal = new PortalAPI(baseUrl)) {
            PortalList list = downloadEntriesWithFactSheet(portal);
            downloadFactSheets(portal, list);
            downloadReferencedContextElements(portal);

            formatter.writeIndexPages(list, referencedProcessContextElements.values());

            logger.info("Successfully saved the process and processcontext data from share \"" + list.title + "\"");
        } catch (RequestFailedException e) {
            logger.error("An error occurred while connecting to the processplatform. HTTP status code " + e.getHttpCode());
        } finally {
            logger.info("Exiting factsheet downloader.");
        }
    }

    //Note: Entries with the notation "PROCESS_MAP" should be
    //      ignored because they do not have a factsheet inside the PPP
    private PortalList downloadEntriesWithFactSheet(PortalAPI portal) {
        PortalList list = portal.loadPortalList(shareToken);
        list.entries.removeIf(entry -> entry.notation == ModelNotation.PROCESS_MAP);
        return list;
    }

    private void downloadFactSheets(PortalAPI portal, PortalList list) throws RequestFailedException {
        logger.info("Downloading factsheets...");

        for (PortalEntry entry : list.entries) {
            FactSheetXML factsheet = portal.loadProcessFactsheetXML(shareToken, entry.id);
            formatter.writeFactSheetPage(factsheet, entry.viewerLink);

            trackReferencedContextElements(factsheet);
        }
    }

    private void downloadReferencedContextElements(PortalAPI portal) throws RequestFailedException {
        logger.info("Downloading referenced context elements...");

        for (ReferenceXML reference : referencedProcessContextElements.values()) {
            ProcessContextElementXML contextElement = portal.loadProcessContextElementXML(shareToken, reference.getTargetType(), reference.getUuid());
            formatter.writeProcessContextPage(reference.getTargetType(), contextElement);
        }
    }

    private void trackReferencedContextElements(FactSheetXML factsheet) {
        for (AttributeGroupXML group : factsheet.getAttributeGroups()) {
            for (AttributeValueXML value : group.getAttributes()) {
                if (value instanceof ReferenceListValueXML) {
                    ReferenceListValueXML references = (ReferenceListValueXML) value;

                    //Note: We need to filter elements here because not
                    //      every reference is a processContextElement
                    referencedProcessContextElements.putAll(
                            references.getReferences()
                                    .stream()
                                    .filter(reference -> processContextTypes.contains(reference.getTargetType()))
                                    .collect(Collectors.toMap(ReferenceXML::getUuid, Function.identity())));
                }
            }
        }
    }
}
