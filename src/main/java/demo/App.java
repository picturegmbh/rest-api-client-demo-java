package demo;

public class App {
    /* URL of the tenant where the share should be downloaded from */
    private static String tenant = "https://www999.prozessplattform.de/musterhausen";

    /* Token for the share that exists on the previously defined tenant */
    private static String shareToken = "76a5e756-6fd7-40fd-ba97-662e03558fe4";

    /* Directory where all the html files will be written to. Directories will be created if they don't exist.*/
    private static String outputDir = "factsheetDownloader/";

    public static void main(String[] args) {
        FactSheetDownloader downloader = new FactSheetDownloader(tenant, shareToken, outputDir);
        downloader.execute();
    }
}
