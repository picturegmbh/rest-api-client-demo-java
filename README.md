# Worum handelt es sich bei diesem Projekt?
In diesem Repository befindet sich der Quellcode zu einem Java-Beispiel-Projekt
zur REST-API der PICTURE-Prozessplattform (PPP). Es handelt sich dabei um eine mit Hilfe des [REST-API-Clients](https://bitbucket.org/picturegmbh/rest_client/) 
umgesetzte Java-Adaption des PHP-Projekts ["Fact Sheet Downloaders"](https://bitbucket.org/picturegmbh/rest-api-client-demo-php/src/master/ "Bitbucket.org").
Das Projekt soll dazu dienen die grundlegenden Funktionen der REST-API/des REST-API-Clients an einem konkreten Beispiel zu veranschaulichen.

Der "fachliche Zweck" der Beispiel-Anwendung "Fact Sheet Downloaders" besteht darin, alle Informationen, welche in einem Mandanten der 
PICTURE-Prozessplattform über eine Portal-Freigabe zugänglich gemacht worden sind, über die REST-API herunterzuladen 
und die Steckbrief-Informationen der einzelnen Informations-Objekte (z.B. Prozesse, Organisationseinheiten, Dokumente) 
in Form von untereinander verlinkten HTML-Dokumenten im lokalen Dateisystem des Hosts, auf dem die Anwendung installiert ist, abzuspeichern. 

Anhand dieses Beispiels werden insbesondere folgende Aspekte der Nutzung der REST-API demonstriert:

* Verwendung des Clients um auf eine Portalfreigabe zuzugreifen.
* Download, Auslesen und Verarbeiten der zentralen Informationsobjekte aus dem Format "PicXML 3" (Prozesse, Prozesskontext-Elemente).

# Hinweise zur Einrichtung des Projektes

Vorausgesetzt wird, dass bereits eine Entwicklungsumgebung (z.B. IntelliJ ooder Eclipse) inkl. Maven (Version 3.5.3 oder 
neuer) sowie ein Java Development Kit (JDK) in Version 8 (oder neuer) installiert sind.
Zur Ausführung des Beispiel-Projektes genügt es, den Quellcode aus diesem Repository in die lokale Entwicklungsumgebung 
zu übernehmen, zu kompilieren und die Main-Methode der Klasse demo.App auszuführen.

**Achtung**: Die Maven-POM-Datei des Projektes enthält Abhängigkeiten zu einigen Maven-Modulen, die nicht aus öffentlichen 
Maven-Repositories erhältlich sind. Um diese auflösen zu können, wird ein Benutzerkonto für das private Maven-Repository 
"developer.prozessplattform.de" benötigt. Dieses ist auf Anfrage beim PICTURE-Support erhältlich (support@picture-gmbh.de). 
Die zur Verfügung gestellten Zugangsdaten müssen in der Datei settings.xml im Maven-Verzeichnis des verwendeten 
Benutzerkontos im Abschnitt "servers" wie folgt eingetragen werden: 

```xml
<servers>
    <server>
        <id>developer.prozessplattform.de</id>
        <username>MEIN_BENUTZERNAME</username>
        <password>MEIN_PASSWORT</password>
    </server>
</servers>
``` 

Optional können zusätzlich folgende Anpassungen im Quellcode vorgenommen werden:

* In der _App.java_ befinden sich die Einstellung für das Ausgabeverzeichnis und die nötigen Einstellungen für den Zugriff auf eine Portalfreigabe. 
Diese können den Wünschen entsprechend angepasst werden.
* Die _log4j.xml_ kann angepasst werden, falls Änderungen am Logging-Verhalten gewünscht sind. 
    (Standardmäßig werden alle Ausgaben in der Konsole geloggt.)
 
Im Erfolgsfall befinden sich nach der Ausführung die erstellten HTML-Dateien im Verzeichnis das in der App.java angegeben wurde. Standardmäßig ist dies im Projektverzeichnis
der Ordner _"factsheetDownloader"_. Als Einstiegsseite dient die dort erstellte _"index.html"_. 

Sollte der Download fehlgeschlagen haben, dann lassen sich mit Hilfe der Logging-Ausgaben aus der Konsole Anhaltspunkte zu dem
vorliegenden Fehler finden.

# Anleitung und Tipps zum Durcharbeiten des Quellcodes
Beim Lesen des Quellcodes lässt sich gut in der folgenden Reihenfolge vorgehen:

* **App.java**: Hier befinden sich der Einstiegspunkt und die Einstellungen für den Downloader.
* **FactSheetDownloader.java**: Diese Klasse dient dem Download von Steckbriefen zu Prozessen und Prozesskontexten.
Die heruntergeladenen Daten werden an den HTMLFormatter zur Formatierung und Speicherung weitergereicht.
Hier lässt sich vor allem nachvollziehen, wie eine Verbindung mit der Prozessplattform über die REST-API abläuft.
Außerdem befinden sich hier schon einige relevante Stellen zum Umgang mit verschiedenen Datentypen.
* **HTMLFormatter.java**: Hier werden die empfangenen Datenobjekte ausgelesen und in eine HTML Darstellung umgewandelt.
Besonders von Bedeutung ist dabei die Funktion "appendAttributeGroupHTML(...)". Diese demonstriert den Umgang mit den
unterschiedlichen Datentypen, die zur Laufzeit der PPP für die einzelnen Steckbrief-Attribute konfiguriert werden können.
* **HTMLFileBuilder.java**: Hilfsklasse. Dient dem Aufbau von HTML-Dateien. Das Verständnis dieser Klasse ist nur im Rahmen des Beispielprojektes bedeutsam und für die Verwendung der REST-API nicht weiter wichtig.

# Weiterführende Informationen
* REST-API-Client der PICTURE-Prozessplattform: https://bitbucket.org/picturegmbh/rest_client/
* Dokumentation der REST-API der PICTURE-Prozessplattform im Online-Informationsportal PPP https://picture.atlassian.net/wiki/spaces/pppdoc320/pages/785449253/REST-API+der+Prozessplattform
* Ergänzende Informationen zum Konzept der Portal-Freigaben: https://picture.atlassian.net/wiki/spaces/pppdoc320/pages/579272846/Prozesse+Prozesslandkarten+im+Portal+ver+ffentlichen
* Ergänzende Informationen zum Konzept des zur Laufzeit konfigurierbaren Metamodells der PPP ("Methodenkonfiguration"): https://picture.atlassian.net/wiki/spaces/pppdoc320/pages/579272964/Arbeitsbereiche+konfigurieren
